(() => {
  'use strict';

  var stage = 0;

  document.addEventListener('DOMContentLoaded', () => {
    stage = 1;
  });

  main();

  function main(){
    if(!document.body)
      return setTimeout(main);

    block();

    function block(){
      var e;

      if(stage === 0)
        return setTimeout(block, 1e3);

      for(e of qsa('.g')){
        if(e.innerHTML.includes('<span>Missing:</span>'))
          e.remove();
      }

      if(e = qs('#center_col')){
        if(!e.innerHTML.includes('(without quotes)')){
          e.classList.add('ublock-safe');
        }else{
          e.remove();
        }
      }
    }
  }

  function qs(a, b=null){
    if(b === null){
      b = a;
      a = document;
    }

    return a.querySelector(b);
  }

  function qsa(a, b=null){
    if(b === null){
      b = a;
      a = document;
    }

    return a.querySelectorAll(b);
  }

  function decode(str){
    return str.split('').
      map(a => a.charCodeAt(0) - 32).
      map(a => 94 - a).
      map(a => String.fromCharCode(32 + a)).
      join('');
  }

  function log(...a){
    console.log(...a);
    return a[a.length - 1];
  }
})();